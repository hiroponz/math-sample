# 数式のサンプル

GitLabのMarkdownで数式を書く場合のサンプルです。

## Markdown

````
This math is inline: $`a^2+b^2=c^2`$.

This math is on a separate line:

```math
a^2+b^2=c^2
```
````

## 出力

This math is inline: $`a^2+b^2=c^2`$.

This math is on a separate line:

```math
a^2+b^2=c^2
```

## リファレンスマニュアル

* [Markdown - Math](https://docs.gitlab.com/ee/user/markdown.html#math)
